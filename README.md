# k9s 

- [https://www.escapelife.site/posts/d91f0590.html](https://www.escapelife.site/posts/d91f0590.html)
- [https://juejin.cn/post/6844904176204382215](https://juejin.cn/post/6844904176204382215)

## 启动相关

- 默认配置文件启动

```shell
k9s
```

- 指定配置文件启动
```shell
k9s --kubeconfig=/srv/.kube/config
```

- 指定命名空间

```shell
k9s -n istio-system 
```

## 资源查看

- 查看deployment

```shell
:deployment
```

- 编辑deployemnt 

```shell
e 
```

- 设置副本数

```shell
s
```

- 查看日志

```shell
l
```

- 回到上一页

```shell
esc
```

- oiwde 

```shell
ctrl + w
```

## 帮助 `?`

```shell
────────────────────────────────────────────────────────────────────────── Help ────────────────────────────────────────────────────────────────────────────┐
│ RESOURCE                                           GENERAL                                               NAVIGATION                                         │
│ <0>                    all                         <ctrl-a>                  Aliases                     <j>                     Down                       │
│ <1>                    istio-system                <esc>                     Back/Clear                  <shift-g>               Goto Bottom                │
│ <2>                    default                     <ctrl-u>                  Command Clear               <g>                     Goto Top                   │
│ <ctrl-l>               Bench Run/Stop              <:cmd>                    Command mode                <h>                     Left                       │
│ <c>                    Copy                        <tab>                     Field Next                  <ctrl-f>                Page Down                  │
│ <ctrl-d>               Delete                      <backtab>                 Field Previous              <ctrl-b>                Page Up                    │
│ <d>                    Describe                    </term>                   Filter mode                 <l>                     Right                      │
│ <e>                    Edit                        <?>                       Help                        <k>                     Up                         │
│ <?>                    Help                        <space>                   Mark                                                                           │
│ <l>                    Logs                        <ctrl-\>                  Mark Clear                                                                     │
│ <p>                    Logs Previous               <ctrl-space>              Mark Range                                                                     │
│ <shift-f>              Port-Forward                <:q>                      Quit                                                                           │
│ <ctrl-r>               Refresh                     <ctrl-r>                  Reload                                                                         │
│ <shift-a>              Sort Age                    <ctrl-s>                  Save                                                                           │
│ <shift-n>              Sort Name                   <ctrl-g>                  Toggle Crumbs                                                                  │
│ <shift-t>              Sort Type                   <ctrl-e>                  Toggle Header                                                                  │
│ <ctrl-z>               Toggle Faults                                                                                                                        │
│ <ctrl-w>               Toggle Wide                                                                                                                          │
│ <enter>                View                                                                                                                                 │
│ <y>                    YAML                                                                                                                                 │
│                                                                                                                                                             │                                         
```